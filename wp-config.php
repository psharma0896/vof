<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vof');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>l}0%^4Rtov8Qq6GXb:-)wG{^ACeWq5XhVIm6nqlmCdK1slI;t]tFKs6{7qkt1O{');
define('SECURE_AUTH_KEY',  ',iHBs^8SQk1#g]X;*Uz9^y&5JL5x0ZGScN$KL0N|K.mj=-!aYtXzy,a<bTg4]RmQ');
define('LOGGED_IN_KEY',    'IjZ2p;OgW16>hRb08.4]Q@1_G]$dVggNUZ~cQe)GJUbDbow|iW1-%80H/smuoF%S');
define('NONCE_KEY',        'Ve|T?!c,2SN`!|YND4W6}#xp7}NCwaZlB`!._y(-BgARk%-KxiC)$2!!GUyIgYa:');
define('AUTH_SALT',        '%JtWgfPYrY]znq<}5M/m(5aWf>7Y>#qW~47O}4?~W{R0aAX]jg5H>h3A6QtXmMsp');
define('SECURE_AUTH_SALT', ',]j}t)7i9J/uL.m$z {~5-}%xHa=`)]Tgp<,Cl-HtxQsRj!&vZNdE0ynmCYx0iWG');
define('LOGGED_IN_SALT',   'XUICi#YW/0NKWdI%miIF@UoO !n+xQ= #u?hBS|!O3.E#|H6PPK{~-#mpX3U`0*4');
define('NONCE_SALT',       'posU6^uG8I3|V9-A2aIPeg!VGG0Fe^x{&mw/EIGUn{Ui7bnGU &ZbF_Im2ciQYCJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
